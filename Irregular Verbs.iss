; -- Components.iss --
; Demonstrates a components-based installation.

; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=�������� ������������ �������
AppVerName=�������� ������������ ������� version 0.1.1
AppPublisher=englishextra.github.io
AppCopyright=� 2015 englishextra.github.io <englishextra@mail.ru>
AppSupportURL=https://englishextra.github.io/
AppUpdatesURL=https://englishextra.github.io/
DefaultDirName={pf}\englishextra.github.io\�������� ������������ �������
DefaultGroupName=englishextra.github.io\�������� ������������ �������
DisableDirPage=yes
DisableProgramGroupPage=yes
UninstallDisplayIcon="{sys}\Irregular Verbs.scr"
WizardImageFile=WizModernImage.bmp
WizardSmallImageFile=WizModernSmallImage.bmp
OutputDir=./
OutputBaseFilename=iv-scr-en-ru-0.1.1-shimansky.biz

[Languages]
Name: ru; MessagesFile: "compiler:Languages\Russian.isl"

[Files]
Source: "Irregular Verbs.scr"; DestDir: "{sys}"
Source: "favicon.ico"; DestDir: "{app}"
Source: "�������� ��������.url"; DestDir: "{app}"
; Flags: uninsneveruninstall

[Registry]
Root: HKCU; Subkey: "Control Panel\Desktop"; ValueType: string; ValueName: "ScreenSaveActive"; ValueData: "1"; Flags: uninsdeletekeyifempty
Root: HKCU; Subkey: "Control Panel\Desktop"; ValueType: string; ValueName: "SCRNSAVE.EXE"; ValueData: "{sys}\IRREGU~1.SCR"; Flags: uninsdeletekeyifempty
; Flags: uninsneveruninstall

[Run]
Filename: "{sys}\Irregular Verbs.scr"; Description: "��������� ��������"; Flags: postinstall nowait skipifsilent

[Icons]
Name: "{userdesktop}\�������� ������������ �������"; Filename: "{sys}\Irregular Verbs.scr"; IconFilename: "{app}\favicon.ico"
Name: "{group}\�������� ������������ �������"; Filename: "{sys}\Irregular Verbs.scr"; IconFilename: "{app}\favicon.ico"
Name: "{group}\������� �������� ������������ �������"; Filename: "{app}\unins000.exe"
Name: "{group}\�������� ��������.url"; Filename: "{app}\�������� ��������.url"
; Flags: uninsneveruninstall
